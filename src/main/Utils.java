package main;

public class Utils {
  // 문자열이 숫자인지 확인하는 메소드
  public static boolean isNumeric(String str) {
    try {
      Double.parseDouble(str);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}