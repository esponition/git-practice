package main;

public class Calcultaor {
  public int add(int a, int b) {
    return a + b;
  }

  public int subtract(int a, int b) {
    return a - b;
  }

  public int multiply(int a, int b) {
    return a * b;
  }

  public int divide(int a, int b) {
    if (b == 0) {
      throw new IllegalArgumentException("Divisor cannot be zero.");
    }

    return a / b;
  }

  public static int min(int a, int b) {
    return a;
  }
}
